﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.UI.ValueConverter {

    internal class FlyoutWidthHelper : System.Windows.Data.IValueConverter {

        public object Convert (object value, Type targetType, object parameter, CultureInfo culture) {
            double ret;
            if (Double.TryParse (value.ToString (), out ret)) // Convertito value in double
                return ret * 0.25; // Flyout largo il 25% della finestra
            return value;
        }

        public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException ();
        }
    }
}