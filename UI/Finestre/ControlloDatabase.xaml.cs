﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LYTM.Core.UI.Finestre {

    /// <summary>
    /// Logica di interazione per ControlloDatabase.xaml
    /// </summary>
    public partial class ControlloDatabase : MahApps.Metro.Controls.MetroWindow, System.ComponentModel.INotifyPropertyChanged {

        #region Costanti

        #endregion Costanti

        #region Variabili

        private int databaseAttuale;
        private int totaleDatabase;
        private bool isRunning;
        private String operazioneCorrente;
        private log4net.ILog logger = log4net.LogManager.GetLogger (typeof (ControlloDatabase));

        #endregion Variabili

        #region Costruttori

        public ControlloDatabase () {
            InitializeComponent ();

            ElencoDatabase = new List<Database.MyDatabase> ();
        }

        #endregion Costruttori

        #region Property

        public Int32 DatabaseAttuale {
            get {
                return databaseAttuale;
            }
            set {
                databaseAttuale = value;
                OnPropertyChanged ();
            }
        }

        public Int32 TotaleDatabase {
            get {
                return totaleDatabase;
            }
            set {
                totaleDatabase = value;
                OnPropertyChanged ();
            }
        }

        public bool IsRunning {
            get {
                return isRunning;
            }
            set {
                isRunning = value;
                OnPropertyChanged ();
            }
        }

        public String OperazioneCorrente {
            get {
                return operazioneCorrente;
            }
            set {
                operazioneCorrente = value;
                OnPropertyChanged ();
            }
        }

        public List<Database.MyDatabase> ElencoDatabase { get; set; }

        #endregion Property

        #region Metodi Pubblici

        #endregion Metodi Pubblici

        #region Metodi Privati

        private void OnPropertyChanged ([System.Runtime.CompilerServices.CallerMemberName] String name = null) {
            Dispatcher.BeginInvoke (new Action (() => {
                if (PropertyChanged != null) {
                    PropertyChanged (this, new System.ComponentModel.PropertyChangedEventArgs (name));
                }
            }));
        }

        #endregion Metodi Privati

        #region Gestione Eventi

        private void ControlloDatabase_Loaded (object sender, RoutedEventArgs e) {
            TotaleDatabase = ElencoDatabase.Count;
            DatabaseAttuale = 0;
            System.Threading.ThreadPool.QueueUserWorkItem ((status) => {
                try {
                    // Inizio a controllare ogni db
                    IsRunning = true;
                    foreach (var db in ElencoDatabase) {
                        logger.Debug (operazioneCorrente);
                        OperazioneCorrente = "Controllo e creazione tabelle";
                        logger.Info ("Controllo e creazione tabelle");
                        db.Database.Initialize (true);
                        db.Dispose ();
                        DatabaseAttuale++;
                    }
                    IsRunning = false;
                } catch (Exception ex) {
                    logger.Error (ex);
                } finally {
                    // Imposto il risultato e esco
                    Dispatcher.Invoke (new Action (() => {
                        // Se isRunning = true allora c'è stato un errore, restituisco false
                        DialogResult = !isRunning;
                        Close ();
                    }));
                }
            });
        }

        #endregion Gestione Eventi

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged
    }
}