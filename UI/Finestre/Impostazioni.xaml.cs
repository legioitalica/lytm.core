﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using log4net;

namespace LYTM.Core.UI.Finestre {

    /// <summary>
    /// Logica di interazione per Impostazioni.xaml
    /// </summary>
    public partial class Impostazioni : MahApps.Metro.SimpleChildWindow.ChildWindow {
        private ILog logger = LogManager.GetLogger (typeof (Impostazioni));

        public Impostazioni () {
            InitializeComponent ();
        }

        private void ChildWindow_Loaded (object sender, RoutedEventArgs e) {
        }
    }
}