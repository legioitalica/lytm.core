﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	UI\Finestre\Template\MainWindow.cs
//
// summary:	Implements the main Windows Form
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.SimpleChildWindow;

namespace LYTM.Core.UI.Finestre {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   The application's main form. </summary>
    ///
    /// <remarks>   Luca, 03/02/2018. </remarks>
    ///
    /// <seealso cref="T:MahApps.Metro.Controls.MetroWindow"/>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public class MainWindow : MahApps.Metro.Controls.MetroWindow {

        #region Variabili

        /// <summary>   Bottone BitBucket. </summary>
        private Button bitbucket;

        /// <summary>   Bottone PayPal. </summary>
        private Button paypal;

        /// <summary>   Bottone Impostazioni. </summary>
        private Button impostazioni;

        /// <summary>   Bottone Gestione FlyOut. </summary>
        private Button btnFlyoutMenu;

        /// <summary>   URL Progetto Bitbucket. </summary>
        private Uri _bitbucketUrl;

        /// <summary>   URL pagina PayPal. </summary>
        private Uri _paypalUrl;

        /// <summary>   Finestra Figlia. </summary>
        private ChildWindow figlio;

        /// <summary>   FlyOut Menu Laterale. </summary>
        private MahApps.Metro.Controls.Flyout flyoutMenuNavigazioneLaterale;

        /// <summary>   Contenuto FlyOut Menu laterale. </summary>
        private ItemsControl viewMenuLaterale;

        #endregion Variabili

        #region Costruttori

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Default constructor. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public MainWindow () {
            // Carico file stile finestra
            ResourceDictionary dic = new ResourceDictionary () { Source = new Uri (@"pack://application:,,,/" + System.Reflection.Assembly.GetExecutingAssembly ().GetName ().Name + ";component/Resources/StileMainWIndows.xaml", UriKind.RelativeOrAbsolute) };

            // Dimensioni minime finestre
            MinWidth = 1000;
            MinHeight = 600;

            // Bottone BitBucket
            bitbucket = new Button () { Content = new MahApps.Metro.IconPacks.PackIconMaterial () { Kind = MahApps.Metro.IconPacks.PackIconMaterialKind.Bitbucket } };
            bitbucket.Click += Bitbucket_Click;
            bitbucket.Visibility = Visibility.Collapsed;

            // Bottone PayPal
            paypal = new Button () { Content = new MahApps.Metro.IconPacks.PackIconModern () { Kind = MahApps.Metro.IconPacks.PackIconModernKind.Paypal } };
            paypal.Click += Paypal_Click;
            paypal.Visibility = Visibility.Collapsed;

            // Bottone Impostazioni
            impostazioni = new Button () { Content = new MahApps.Metro.IconPacks.PackIconMaterial () { Kind = MahApps.Metro.IconPacks.PackIconMaterialKind.Settings } };
            impostazioni.Click += Impostazioni_Click;
            impostazioni.Visibility = Visibility.Visible;

            // Bottone Apertura Flyout
            btnFlyoutMenu = new Button () { Content = new MahApps.Metro.IconPacks.PackIconMaterial () { Kind = MahApps.Metro.IconPacks.PackIconMaterialKind.Menu } };
            btnFlyoutMenu.Click += BtnFlyoutMenu_Click;
            btnFlyoutMenu.Visibility = Visibility.Visible;

            // Flyout Menu Navigazione Laterale
            flyoutMenuNavigazioneLaterale = new MahApps.Metro.Controls.Flyout ();
            System.Windows.Data.Binding bind = new System.Windows.Data.Binding ("ActualWidth");
            bind.Source = this;
            bind.Converter = new ValueConverter.FlyoutWidthHelper ();
            bind.Mode = System.Windows.Data.BindingMode.OneWay;
            bind.UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged;
            flyoutMenuNavigazioneLaterale.SetBinding (MahApps.Metro.Controls.Flyout.WidthProperty, bind);
            var dockPanel = new DockPanel ();
            viewMenuLaterale = new ItemsControl ();
            viewMenuLaterale.Style = (System.Windows.Style) dic["StileListaMenuPrincipale"];
            var groupStyle = new GroupStyle ();
            groupStyle.ContainerStyle = (System.Windows.Style) dic["StileRaggruppamentoListaMenuPrincipale"];
            viewMenuLaterale.GroupStyle.Add (groupStyle);
            dockPanel.Children.Add (viewMenuLaterale);
            flyoutMenuNavigazioneLaterale.Content = dockPanel;

            // Barra Menu Destra
            this.RightWindowCommands = new MahApps.Metro.Controls.WindowCommands ();
            this.RightWindowCommands.Visibility = Visibility.Visible;
            this.LeftWindowCommandsOverlayBehavior = MahApps.Metro.Controls.WindowCommandsOverlayBehavior.Flyouts;

            inizializzaBarraMenuDestra ();

            // Barra Menu Sinistra
            this.LeftWindowCommands = new MahApps.Metro.Controls.WindowCommands ();
            this.LeftWindowCommands.Visibility = Visibility.Visible;
            this.LeftWindowCommandsOverlayBehavior = MahApps.Metro.Controls.WindowCommandsOverlayBehavior.Always;

            inizializzaBarraMenuSinistra ();

            // Flyouts
            this.Flyouts = new MahApps.Metro.Controls.FlyoutsControl ();
            Flyouts.Items.Add (flyoutMenuNavigazioneLaterale);
        }

        #endregion Costruttori

        #region Property

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Eventuale link alla pagina BitBucket del progetto, se settato visualizza il pulsante
        ///     BitBucket sulla destra della barra del titolo.
        /// </summary>
        ///
        /// <value> The bit bucket URL. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Category ("LYTM")]
        [Description ("Eventuale link alla pagina BitBucket del progetto, se settato visualizza il pulsante BitBucket sulla destra della barra del titolo")]
        public Uri BitBucketUrl {
            get {
                return _bitbucketUrl;
            }
            set {
                _bitbucketUrl = value;
                bitbucket.Visibility = _bitbucketUrl == null ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Eventuale link alla pagina PayPal per donazioni al progetto, se settato visualizza il
        ///     pulsante PayPal sulla destra nella barra del titolo.
        /// </summary>
        ///
        /// <value> The pay palette URL. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Category ("LYTM")]
        [Description ("Eventuale link alla pagina PayPal per donazioni al progetto, se settato visualizza il pulsante PayPal sulla destra nella barra del titolo")]
        public Uri PayPalUrl {
            get {
                return _paypalUrl;
            }
            set {
                _paypalUrl = value;
                paypal.Visibility = _paypalUrl == null ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the right window commands. </summary>
        ///
        /// <value> The right window commands. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Browsable (false)]
        public new MahApps.Metro.Controls.WindowCommands RightWindowCommands {
            get {
                return base.RightWindowCommands;
            }
            set {
                base.RightWindowCommands = value;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Lista di elementi presenti sulla destra nella barra del titolo. </summary>
        ///
        /// <value> The barra menu destra. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Category ("LYTM")]
        [Description ("Lista di elementi presenti sulla destra nella barra del titolo")]
        public ItemCollection BarraMenuDestra {
            get {
                return RightWindowCommands.Items;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Lista di elementi presenti sulla sinistra nella barra del titolo. </summary>
        ///
        /// <value> The barra menu sinistra. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Category ("LYTM")]
        [Description ("Lista di elementi presenti sulla sinistra nella barra del titolo")]
        public ItemCollection BarraMenuSinistra {
            get {
                return LeftWindowCommands.Items;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the left window commands. </summary>
        ///
        /// <value> The left window commands. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Browsable (false)]
        public new MahApps.Metro.Controls.WindowCommands LeftWindowCommands {
            get {
                return base.LeftWindowCommands;
            }
            set {
                base.LeftWindowCommands = value;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the width of the minimum. </summary>
        ///
        /// <value> The width of the minimum. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public new double MinWidth {
            get {
                return base.MinWidth;
            }
            set {
                if (value < 1000)
                    base.MinWidth = 1000;
                else
                    base.MinWidth = value;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the height of the minimum. </summary>
        ///
        /// <value> The height of the minimum. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public new double MinHeight {
            get {
                return base.MinHeight;
            }
            set {
                if (value < 700)
                    base.MinHeight = 700;
                else
                    base.MinHeight = value;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the flyouts. </summary>
        ///
        /// <value> The flyouts. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public new MahApps.Metro.Controls.FlyoutsControl Flyouts {
            get {
                return base.Flyouts;
            }
            private set {
                base.Flyouts = value;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Lista di plugin caricati dall'applicazione, crea il menu di navigazione laterale.
        /// </summary>
        ///
        /// <value> The elementi menu laterale. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Category ("LYTM")]
        [Description ("Lista di plugin caricati dall'applicazione, crea il menu di navigazione laterale")]
        public System.Collections.ObjectModel.ReadOnlyObservableCollection<Plugin.IPlugin> ElementiMenuLaterale {
            get {
                return (System.Collections.ObjectModel.ReadOnlyObservableCollection<Plugin.IPlugin>) viewMenuLaterale.ItemsSource;
            }
            set {
                viewMenuLaterale.ItemsSource = value;
            }
        }

        #endregion Property

        #region Funzioni Pubbliche

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Apri finestra figlio. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="finestra">
        ///     The finestra.
        /// </param>
        ///
        /// <returns>   A Task&lt;bool&gt; </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public async Task<bool> ApriFinestraFiglio (ChildWindow finestra) {
            if (figlio != null) // Ho già una finestra aperta, non posso aprirne un'altra
                return false;

            // Apro la finestra
            figlio = finestra;
            figlio.ClosingFinished += Figlio_ClosingFinished;
            await this.ShowChildWindowAsync (figlio);
            return true;
        }

        #endregion Funzioni Pubbliche

        #region Funzioni Private

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializza barra menu destra. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void inizializzaBarraMenuDestra () {
            this.RightWindowCommands.Items.Add (bitbucket);
            this.RightWindowCommands.Items.Add (paypal);
            this.RightWindowCommands.Items.Add (impostazioni);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializza barra menu sinistra. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void inizializzaBarraMenuSinistra () {
            this.LeftWindowCommands.Items.Add (btnFlyoutMenu);
        }

        #endregion Funzioni Private

        #region Gestione Eventi

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by Bitbucket for click events. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Routed event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void Bitbucket_Click (object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start (_bitbucketUrl.ToString ());
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by Paypal for click events. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Routed event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void Paypal_Click (object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start (_paypalUrl.ToString ());
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by Impostazioni for click events. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Routed event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private async void Impostazioni_Click (object sender, RoutedEventArgs e) {
            if (figlio != null)
                return;

            await ApriFinestraFiglio (new Impostazioni ());
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by Figlio for closing finished events. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Routed event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void Figlio_ClosingFinished (object sender, RoutedEventArgs e) {
            figlio = null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by BtnFlyoutMenu for click events. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Routed event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void BtnFlyoutMenu_Click (object sender, RoutedEventArgs e) {
            flyoutMenuNavigazioneLaterale.IsOpen = !flyoutMenuNavigazioneLaterale.IsOpen;
        }

        #endregion Gestione Eventi
    }
}