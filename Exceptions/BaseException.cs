﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Exceptions {

    public class BaseException : Exception {
        public int ErrorCode { get; private set; }

        public BaseException (int errorCode) : base () {
            ErrorCode = errorCode;
        }

        public BaseException (String message, int errorCode) : base (message) {
            ErrorCode = errorCode;
        }

        public BaseException (String message, System.Exception innerException, int errorCode) : base (message, innerException) {
            ErrorCode = errorCode;
        }

        public BaseException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context, int errorCode) : base (info, context) {
            ErrorCode = errorCode;
        }

        public override string ToString () {
            return "Error Code: " + ErrorCode + "\n" + base.ToString ();
        }
    }
}