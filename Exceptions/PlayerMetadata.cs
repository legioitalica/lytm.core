﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Exceptions {

    internal class PlayerMetadata {
        public String Nome { get; set; }
        public String Server { get; set; }
        public bool Completato { get; set; }
    }
}