﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Exceptions {

    internal class ErrorCode {
        public const int NoError = 0x00100000;
        public const int ErroreConversione = 0x00100001;
        public const int ErroreControlloDatabase = 0x00100002;
        public const int UnknownError = 0x001FFFFFF;
    }
}