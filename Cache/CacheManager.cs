﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LYTM.Core.Cache {

    public class CacheManager {

        #region Variabili

        private ILog logger = LogManager.GetLogger (typeof (CacheManager));

        private static CacheManager _istance;
        private Dictionary<String, Plugin.IDownloader> downloadPlugins;

        #endregion Variabili

        #region Costruttori

        private CacheManager () {
            downloadPlugins = new Dictionary<String, Plugin.IDownloader> ();
        }

        #endregion Costruttori

        #region Property

        public static CacheManager Istance {
            get {
                if (_istance == null)
                    _istance = new CacheManager ();

                return _istance;
            }
        }

        #endregion Property

        #region Metodi Pubblici

        public List<String> ElencaFile (String url) {
            List<String> ret = new List<String> ();

            url = Utility.UrlLengthen (url);
            Uri uri = new Uri (url);
            String domain = uri.Host;
            var plugin = downloadPlugins.ContainsKey (domain) ? downloadPlugins[domain] : null;
            if (plugin != null)
                ret = plugin.ElencaFiles (url);

            return ret;
        }

        #endregion Metodi Pubblici

        #region Metodi Interni

        internal void AddDownloadPlugins (Dictionary<String, Plugin.IDownloader> downloadPlugins) {
            this.downloadPlugins = this.downloadPlugins.Union (downloadPlugins).ToDictionary (pair => pair.Key, pair => pair.Value);
        }

        #endregion Metodi Interni
    }
}