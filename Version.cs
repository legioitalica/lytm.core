namespace LYTM.Core {

    internal class Version {
        public static readonly System.Version number = new System.Version (1,0,0,194);
        public const string numberWithRevString = "1.0.0.194";
        public const string numberString = "1.0.094";
        public const string branchName = "feature/Plugin_Getsione_Magazzino";
        public const string branchSha1 = "8d228e3";
        public const string branchRevCount = "9";
        public const string informational = "1.0.0.194 [ 8d228e3 ]";
        public const string informationalFull = "1.0.0.194 [ 8d228e3 ] /'feature/Plugin_Getsione_Magazzino':9";
    }
}
