﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Impostazioni {

    public class Impostazioni : IImpostazioni {

        #region Variabili

        /// <summary>   Istanza di impostazioni. </summary>
        private static Impostazioni instance;

        /// <summary>   The impostazioni applicazione. </summary>
        private IImpostazioni _impostazioniApplicazione;

        /// <summary>   The livello log. </summary>
        private int _livelloLog;

        #endregion Variabili

        #region Costruttori

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Constructor that prevents a default instance of this class from being created.
        /// </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        [Newtonsoft.Json.JsonConstructor]
        private Impostazioni () {
        }

        #endregion Costruttori

        #region Property

        public static Impostazioni Istanza {
            get {
                if (instance == null) {
                    if (System.IO.File.Exists (Utility.getConfigFileName ())) {
                        using (var stream = System.IO.File.OpenRead (Utility.getConfigFileName ())) {
                            using (var reader = new System.IO.StreamReader (stream)) {
                                var imp = reader.ReadToEnd ();
                                instance = Newtonsoft.Json.JsonConvert.DeserializeObject<Impostazioni> (imp);
                            }
                        }
                    } else {
                        instance = new Impostazioni ();
                        var settings = Newtonsoft.Json.JsonConvert.SerializeObject (instance, Newtonsoft.Json.Formatting.Indented);
                        System.IO.File.WriteAllText (Utility.getConfigFileName (), settings);
                    }
                }

                return instance;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottieni o imposta le impostazioni relative all'applicazione </summary>
        ///
        /// <value> Impostazioni dell'Applicazione. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public IImpostazioni ImpostazioniApplicazione {
            get {
                return _impostazioniApplicazione;
            }
            set {
                _impostazioniApplicazione = value;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottieni o imposta il livello log. </summary>
        ///
        /// <value> The livello log. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public int LivelloLog {
            get {
                return _livelloLog;
            }
            set {
                _livelloLog = value;
                OnPropertyChanged ();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottieni o imposta le impostazioni relative al database applicazione </summary>
        ///
        /// <value> Impostazioni database. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public ImpostazioniDatabase ImpostazioniDatabase { get; internal set; } = new ImpostazioniDatabase ();

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary> Ottieni o imposta le impostazioni Relative ai Plugin </summary>
        ///
        /// <value> Impostazioni Plugin. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        internal Dictionary<Guid, IImpostazioni> ImpostazioniPlugin { get; } = new Dictionary<Guid, IImpostazioni> ();

        #endregion Property

        #region Implementazione IImpostazioni

        public event EventHandler ImpostazioniAggiornate;

        protected void OnPropertyChanged ([System.Runtime.CompilerServices.CallerMemberName] String name = null) {
            if (ImpostazioniAggiornate != null) {
                ImpostazioniAggiornate (this, new System.ComponentModel.PropertyChangedEventArgs (name));
            }
        }

        #endregion Implementazione IImpostazioni
    }
}