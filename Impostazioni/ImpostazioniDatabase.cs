﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	impostazioni\impostazionidatabase.cs
//
// summary:	Implements the impostazionidatabase class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Impostazioni {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Impostazioni per Database. </summary>
    ///
    /// <remarks>   Luca, 04/02/2018. </remarks>
    ///
    /// <seealso cref="T:LYTM.Core.Impostazioni.IImpostazioni"/>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public class ImpostazioniDatabase : IImpostazioni {

        #region Variabili

        /// <summary>   The database. </summary>
        private TipoDatabase _database = TipoDatabase.SQLite;

        private IImpostazioni impostazioniConnessione;

        #endregion Variabili

        #region Property

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Tipo di database da utilizzare. </summary>
        ///
        /// <value> The database. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public TipoDatabase Database {
            get {
                return _database;
            }
            set {
                _database = value;
                OnPropertyChanged ();
            }
        }

        public IImpostazioni ImpostazioniConnessione {
            get {
                return impostazioniConnessione;
            }
            set {
                if (impostazioniConnessione != null)
                    impostazioniConnessione.ImpostazioniAggiornate -= ImpostazioniConnessione_ImpostazioniAggiornate;
                impostazioniConnessione = value;
                if (impostazioniConnessione != null)
                    impostazioniConnessione.ImpostazioniAggiornate += ImpostazioniConnessione_ImpostazioniAggiornate;
            }
        }

        #endregion Property

        #region Implementazione IImpostazioni

        /// <summary>   Event queue for all listeners interested in ImpostazioniAggiornate events. </summary>
        public event EventHandler ImpostazioniAggiornate;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Executes the property changed action. </summary>
        ///
        /// <remarks>   Luca, 04/02/2018. </remarks>
        ///
        /// <param name="name">
        ///     The name.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        protected void OnPropertyChanged ([System.Runtime.CompilerServices.CallerMemberName] String name = null) {
            if (ImpostazioniAggiornate != null) {
                ImpostazioniAggiornate (this, new System.ComponentModel.PropertyChangedEventArgs (name));
            }
        }

        #endregion Implementazione IImpostazioni

        #region Eventi

        private void ImpostazioniConnessione_ImpostazioniAggiornate (object sender, EventArgs e) {
            System.ComponentModel.PropertyChangedEventArgs ev = (System.ComponentModel.PropertyChangedEventArgs) e;
            OnPropertyChanged (ev.PropertyName);
        }

        #endregion Eventi

        #region Classi Interne

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Values that represent tipo databases. </summary>
        ///
        /// <remarks>   Luca, 04/02/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public enum TipoDatabase {

            /// <summary>   Database SQLite. </summary>
            [System.ComponentModel.Description ("SQLite")]
            SQLite,

            /// <summary>   Database MySQL. </summary>
            [System.ComponentModel.Description ("MySql")]
            MySql,

            /// <summary>   Database Microsoft SQL Server. </summary>
            [System.ComponentModel.Description ("Microsoft SQL Server")]
            SqlServer
        }

        #endregion Classi Interne
    }
}