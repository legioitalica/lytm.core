﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	utility.cs
//
// summary:	Classe statica con funzioni di aiuto
////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Classe con funzioni di aiuto. </summary>
    ///
    /// <remarks>   Luca, 03/02/2017. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static class Utility {

        /// <summary>   Suffissi di peso. </summary>
        private static readonly String[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        /// <summary>   The logger. </summary>
        private static log4net.ILog logger = log4net.LogManager.GetLogger (typeof (Utility));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene la cartella dell'applicazione in <b>%appdata%.</b>
        ///             <para>Se la cartella non è presente viene creata</para>
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   La cartella dell'applicazione in %appdata%. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getAppDataFolder () {
            // Vecchio
            //String path = System.IO.Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), @"Legio YouTube Manager");
            var versionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo (System.Reflection.Assembly.GetEntryAssembly ().Location);
            String path;
            if (!String.IsNullOrWhiteSpace (versionInfo.CompanyName))
                path = System.IO.Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), versionInfo.CompanyName, versionInfo.ProductName);
            else
                path = System.IO.Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), versionInfo.ProductName);
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene la cartella di cache. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Full path della cartella di cache. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getCacheFolder () {
            String path = System.IO.Path.Combine (getAppDataFolder (), "cache");
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        public static String getTempFolder () {
            String path = System.IO.Path.Combine (getAppDataFolder (), "temp");
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome del file di configurazione. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome del file di configurazione completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getConfigFileName () {
            return System.IO.Path.Combine (getAppDataFolder (), @"config.json");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome del database. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome del database completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getDatabaseFileName () {
            return System.IO.Path.Combine (getAppDataFolder (), @"data.db");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene la cartella dei plugins. </summary>
        ///
        /// <remarks>   Luca, 19/08/2017. </remarks>
        ///
        /// <returns>   Cartella dei plugins. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getPluginDir () {
            String path = System.IO.Path.Combine (getAppDataFolder (), "plugins");
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il valore con dimensione aggiunta. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="value">
        ///     Valore in byte da convertire.
        /// </param>
        /// <param name="decimalPlaces">
        ///     Numero di posizioni decimali.
        /// </param>
        ///
        /// <returns>   Stringa in formato {valore} {dimensione}. (es 1.25 GB) </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String sizeSuffix (Int64 value, int decimalPlaces = 1) {
            if (value < 0) { return "-" + sizeSuffix (-value); }
            if (value == 0) { return "0.0 bytes"; }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int) Math.Log (value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag)
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal) value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round (adjustedSize, decimalPlaces) >= 1000) {
                mag += 1;
                adjustedSize /= 1024;
            }

            return String.Format ("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome del file di log. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome del file di log completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getLogFileName () {
            String path = System.IO.Path.Combine (getAppDataFolder (), @"log");

            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            //return System.IO.Path.Combine (path, "LegioVideoUploader.log");
            return System.IO.Path.Combine (path, System.Reflection.Assembly.GetEntryAssembly ().GetName ().Name + ".log");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome dell'archivio zip dei file di log. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome dell'archivio zip dei file di log completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getLogZipFileName () {
            String path = System.IO.Path.Combine (getAppDataFolder (), @"log");

            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            // return System.IO.Path.Combine (path, "LegioVideoUploader.log.zip");
            return System.IO.Path.Combine (path, System.Reflection.Assembly.GetEntryAssembly ().GetName ().Name + ".log.zip");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Windows.Controls.RichTextBox che aggiunge un testo alla
        ///     fine.
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="box">
        ///     Il System.Windows.Controls.RichTextBox a cui aggiungere il testo.
        /// </param>
        /// <param name="text">
        ///     Testo da aggiungere.
        /// </param>
        /// <param name="color">
        ///     Colore del testo.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void AppendText (this System.Windows.Controls.RichTextBox box, String text, System.Windows.Media.SolidColorBrush color) {
            System.Windows.Media.BrushConverter bc = new System.Windows.Media.BrushConverter ();
            System.Windows.Documents.TextRange tr = new System.Windows.Documents.TextRange (box.Document.ContentEnd, box.Document.ContentEnd);
            tr.Text = text;
            tr.ApplyPropertyValue (System.Windows.Documents.TextElement.BackgroundProperty, color);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Collections.Specialized.OrderedDictionary che trova
        ///     l'indice di una specifica chiave
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="dic">
        ///     OrderedDictionary in cui cercare.
        /// </param>
        /// <param name="key">
        ///     Chiave da cercare.
        /// </param>
        ///
        /// <returns>   The zero-based index of the found key, or -1 if no match was found. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static int indexOfKey (this System.Collections.Specialized.OrderedDictionary dic, Object key) {
            int index = 0;
            Object[] keyCollection = new Object[dic.Count];
            dic.Keys.CopyTo (keyCollection, 0);
            for (; index < keyCollection.Length; index++)
                if (keyCollection[index].Equals (key))
                    return index;
            return -1;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Un metodo di estensione per System.IO.DirectoryInfo che restituisce il peso in byte
        ///             della cartella.
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="dir">
        ///     Cartella da pesare.
        /// </param>
        ///
        /// <returns>   Peso della cartella in byte. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static long ByteSize (this System.IO.DirectoryInfo dir) {
            if (!dir.Exists)
                return 0;
            long size = 0;
            // Add file sizes.
            System.IO.FileInfo[] fis = dir.GetFiles ();
            foreach (System.IO.FileInfo fi in fis) {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            System.IO.DirectoryInfo[] dis = dir.GetDirectories ();
            foreach (System.IO.DirectoryInfo di in dis) {
                size += di.ByteSize ();
            }
            return size;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Windows.DependencyObject che restituisce un figlio del
        ///     tipo selezionato.
        /// </summary>
        ///
        /// <remarks>   Luca, 06/02/2017. </remarks>
        ///
        /// <typeparam name="T">
        ///     Tipo del figlio richiesto.
        /// </typeparam>
        /// <param name="depObj">
        ///     Oggetto padre su cui cercare.
        /// </param>
        ///
        /// <returns>   Il figlio di tipo T. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static T GetChildOfType<T> (this System.Windows.DependencyObject depObj) where T : System.Windows.DependencyObject {
            if (depObj == null) return null;

            for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount (depObj); i++) {
                var child = System.Windows.Media.VisualTreeHelper.GetChild (depObj, i);

                var result = (child as T) ?? GetChildOfType<T> (child);
                if (result != null) return result;
            }
            return null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene ID imgur da link. </summary>
        ///
        /// <remarks>   Luca, 03/03/2017. </remarks>
        ///
        /// <param name="imgurLink">
        ///     Link a immagine/album imgur.
        /// </param>
        ///
        /// <returns>   ID immagine/album imgur. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String ImgurID (String imgurLink) {
            return imgurLink.Substring (imgurLink.LastIndexOf ('/'));
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Windows.DependencyObject che rimuove un figlio.
        /// </summary>
        ///
        /// <remarks>   Luca, 04/03/2017. </remarks>
        ///
        /// <param name="parent">
        ///     Il padre.
        /// </param>
        /// <param name="child">
        ///     Figlio da rimuovere.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void RemoveChild (this System.Windows.DependencyObject parent, System.Windows.UIElement child) {
            var panel = parent as System.Windows.Controls.Panel;
            if (panel != null) {
                panel.Children.Remove (child);
                return;
            }

            var decorator = parent as System.Windows.Controls.Decorator;
            if (decorator != null) {
                if (decorator.Child == child) {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as System.Windows.Controls.ContentPresenter;
            if (contentPresenter != null) {
                if (contentPresenter.Content == child) {
                    contentPresenter.Content = null;
                }
                return;
            }

            var contentControl = parent as System.Windows.Controls.ContentControl;
            if (contentControl != null) {
                if (contentControl.Content == child) {
                    contentControl.Content = null;
                }
                return;
            }

            // maybe more
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Espande l'url. </summary>
        ///
        /// <remarks>   Luca, 26/08/2017. </remarks>
        ///
        /// <param name="url">
        ///     Url da espandere.
        /// </param>
        ///
        /// <returns>   Url espansa. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string UrlLengthen (string url) {
            string newurl = url;

            bool redirecting = true;

            while (redirecting) {
                try {
                    System.Net.HttpWebRequest request = (System.Net.HttpWebRequest) System.Net.WebRequest.Create (newurl);
                    request.AllowAutoRedirect = false;
                    request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 4.0.20506)";
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse) request.GetResponse ();
                    if ((int) response.StatusCode == 301 || (int) response.StatusCode == 302) {
                        string uriString = response.Headers["Location"];
                        logger.Debug ("Redirecting " + newurl + " to " + uriString + " because " + response.StatusCode);
                        newurl = uriString;
                        // and keep going
                    } else {
                        logger.Debug ("Not redirecting " + url + " because " + response.StatusCode);
                        redirecting = false;
                    }
                } catch (Exception ex) {
                    ex.Data.Add ("url", newurl);
                    logger.Error (ex);
                    redirecting = false;
                }
            }
            return newurl;
        }

        #region ID

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets CPU identifier. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <returns>   The CPU identifier. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GetCPUID () {
            System.Management.ManagementObjectCollection mbsList = null;
            System.Management.ManagementObjectSearcher mbs = new System.Management.ManagementObjectSearcher ("Select * From Win32_processor");
            mbsList = mbs.Get ();
            string id = "";
            foreach (System.Management.ManagementObject mo in mbsList) {
                id = mo["ProcessorID"].ToString ();
            }
            return id;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets HDD identifier. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="hdd">
        ///     The HDD.
        /// </param>
        ///
        /// <returns>   The HDD identifier. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getHddId (String hdd) {
            System.Management.ManagementObject dsk = new System.Management.ManagementObject (String.Format (@"win32_logicaldisk.deviceid=""{0}:""", hdd));
            dsk.Get ();
            return dsk["VolumeSerialNumber"].ToString ();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets motherboard identifier. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <returns>   The motherboard identifier. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getMotherboardId () {
            System.Management.ManagementObjectSearcher mos = new System.Management.ManagementObjectSearcher ("SELECT * FROM Win32_BaseBoard");
            System.Management.ManagementObjectCollection moc = mos.Get ();
            String serial = "";
            foreach (System.Management.ManagementObject mo in moc) {
                serial = (string) mo["SerialNumber"];
            }
            return serial;
        }

        /// <summary>   Unique identifier. </summary>
        private static System.Numerics.BigInteger _uniqueID;

        /// <summary>   The unique identifier string. </summary>
        private static String _uniqueIDStr;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets a unique identifier. </summary>
        ///
        /// <value> The identifier of the unique. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static System.Numerics.BigInteger UniqueID {
            get {
                if (_uniqueID == null) {
                    System.Numerics.BigInteger cpu = System.Numerics.BigInteger.Parse (GetCPUID (), System.Globalization.NumberStyles.HexNumber);
                    System.Numerics.BigInteger hdd = System.Numerics.BigInteger.Parse (getHddId ("C"), System.Globalization.NumberStyles.HexNumber);
                    System.Numerics.BigInteger mb = System.Numerics.BigInteger.Parse (getMotherboardId (), System.Globalization.NumberStyles.HexNumber);

                    _uniqueID = cpu + hdd * System.Numerics.BigInteger.Pow (10, 9) + mb * System.Numerics.BigInteger.Pow (10, 18);
                }
                return _uniqueID;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets the unique identifier string. </summary>
        ///
        /// <value> The unique identifier string. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String UniqueIDStr {
            get {
                if (String.IsNullOrEmpty (_uniqueIDStr)) {
                    _uniqueIDStr = GenerateSHA256 (UniqueID).ToBase62 ().Replace ("=", "");
                }
                return _uniqueIDStr;
            }
        }

        #endregion ID

        #region Codifice

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Genera lo SHA256 da un biginteger. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="input">
        ///     Input.
        /// </param>
        ///
        /// <returns>   An array of byte. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static byte[] GenerateSHA256 (System.Numerics.BigInteger input) {
            System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256Managed.Create ();
            byte[] hash = sha256.ComputeHash (input.ToByteArray ());
            return hash;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Genera una Stringa SHA256 partendo da un biginteger. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="input">
        ///     Input.
        /// </param>
        /// <param name="base64">
        ///     true to base 64.
        /// </param>
        /// <param name="padding">
        ///     true to padding.
        /// </param>
        ///
        /// <returns>   The sha 256 string. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GenerateSHA256String (System.Numerics.BigInteger input, bool base64 = false, bool padding = false) {
            byte[] hash = GenerateSHA256 (input);
            if (base64) {
                return GetBase64StringFromHash (hash, padding);
            } else
                return GetHexStringFromHash (hash);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Genera lo SHA256 da una stringa. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="inputString">
        ///     The input string.
        /// </param>
        ///
        /// <returns>   An array of byte. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static byte[] GenerateSHA256 (String inputString) {
            System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256Managed.Create ();
            byte[] bytes = Encoding.UTF32.GetBytes (inputString);
            return sha256.ComputeHash (bytes);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Genera una Stringa SHA256 partendo da una stringa. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="inputString">
        ///     The input string.
        /// </param>
        /// <param name="base64">
        ///     true to base 64.
        /// </param>
        /// <param name="padding">
        ///     true to padding.
        /// </param>
        ///
        /// <returns>   The sha 256 string. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GenerateSHA256String (String inputString, bool base64 = false, bool padding = false) {
            byte[] hash = GenerateSHA256 (inputString);
            if (base64) {
                return GetBase64StringFromHash (hash, padding);
            } else
                return GetHexStringFromHash (hash);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Genera una stringa SHA512 da un biginteger. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="input">
        ///     Input.
        /// </param>
        /// <param name="base64">
        ///     true to base 64.
        /// </param>
        /// <param name="padding">
        ///     true to padding.
        /// </param>
        ///
        /// <returns>   The sha 512 string. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GenerateSHA512String (System.Numerics.BigInteger input, bool base64 = false, bool padding = false) {
            System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512Managed.Create ();
            byte[] hash = sha512.ComputeHash (input.ToByteArray ());
            if (base64) {
                return GetBase64StringFromHash (hash, padding);
            } else
                return GetHexStringFromHash (hash);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Genera una stringa SHA512 da una stringa. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="inputString">
        ///     The input string.
        /// </param>
        /// <param name="base64">
        ///     true to base 64.
        /// </param>
        /// <param name="padding">
        ///     true to padding.
        /// </param>
        ///
        /// <returns>   The sha 512 string. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GenerateSHA512String (String inputString, bool base64 = false, bool padding = false) {
            System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512Managed.Create ();
            byte[] bytes = Encoding.UTF32.GetBytes (inputString);
            byte[] hash = sha512.ComputeHash (bytes);
            if (base64) {
                return GetBase64StringFromHash (hash, padding);
            } else
                return GetHexStringFromHash (hash);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene una stringa esadecimale da un Hash. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="hash">
        ///     The hash.
        /// </param>
        ///
        /// <returns>   The string from hash. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GetHexStringFromHash (byte[] hash) {
            StringBuilder result = new StringBuilder ();
            for (int i = 0; i < hash.Length; i++) {
                result.Append (hash[i].ToString ("X2"));
            }
            return result.ToString ();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene una stringa in base64 da un hash. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="hash">
        ///     The hash.
        /// </param>
        /// <param name="padding">
        ///     true to padding.
        /// </param>
        ///
        /// <returns>   The base 64 string from hash. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String GetBase64StringFromHash (byte[] hash, bool padding) {
            if (padding)
                return Convert.ToBase64String (hash);
            else
                return Convert.ToBase64String (hash).Replace ("=", "");
        }

        /// <summary>   The base 62 coding space. </summary>
        private static String Base62CodingSpace = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        /// <summary>
        /// Convert a byte array
        /// </summary>
        /// <param name="original">Byte array</param>
        /// <returns>Base62 string</returns>
        public static String ToBase62 (this byte[] original) {
            StringBuilder sb = new StringBuilder ();
            BitStream stream = new BitStream (original);         // Set up the BitStream
            byte[] read = new byte[1];                          // Only read 6-bit at a time
            while (true) {
                read[0] = 0;
                int length = stream.Read (read, 0, 6);           // Try to read 6 bits
                if (length == 6)                                // Not reaching the end
                {
                    if ((int) (read[0] >> 3) == 0x1f)            // First 5-bit is 11111
                    {
                        sb.Append (Base62CodingSpace[61]);
                        stream.Seek (-1, System.IO.SeekOrigin.Current);    // Leave the 6th bit to next group
                    } else if ((int) (read[0] >> 3) == 0x1e)       // First 5-bit is 11110
                      {
                        sb.Append (Base62CodingSpace[60]);
                        stream.Seek (-1, System.IO.SeekOrigin.Current);
                    } else                                        // Encode 6-bit
                      {
                        sb.Append (Base62CodingSpace[(int) (read[0] >> 2)]);
                    }
                } else if (length == 0)                           // Reached the end completely
                  {
                    break;
                } else                                            // Reached the end with some bits left
                  {
                    // Padding 0s to make the last bits to 6 bit
                    sb.Append (Base62CodingSpace[(int) (read[0] >> (int) (8 - length))]);
                    break;
                }
            }
            return sb.ToString ();
        }

        /// <summary>
        /// Convert a Base62 string to byte array
        /// </summary>
        /// <param name="base62">Base62 string</param>
        /// <returns>Byte array</returns>
        public static byte[] FromBase62 (this String base62) {
            // Character count
            int count = 0;

            // Set up the BitStream
            BitStream stream = new BitStream (base62.Length * 6 / 8);

            foreach (char c in base62) {
                // Look up coding table
                int index = Base62CodingSpace.IndexOf (c);

                // If end is reached
                if (count == base62.Length - 1) {
                    // Check if the ending is good
                    int mod = (int) (stream.Position % 8);
                    if (mod == 0)
                        throw new Exceptions.BaseException ("", new System.IO.InvalidDataException ("an extra character was found"), Exceptions.ErrorCode.ErroreConversione);

                    if ((index >> (8 - mod)) > 0)
                        throw new Exceptions.BaseException ("", new System.IO.InvalidDataException ("invalid ending character was found"), Exceptions.ErrorCode.ErroreConversione);

                    stream.Write (new byte[] { (byte) (index << mod) }, 0, 8 - mod);
                } else {
                    // If 60 or 61 then only write 5 bits to the stream, otherwise 6 bits.
                    if (index == 60) {
                        stream.Write (new byte[] { 0xf0 }, 0, 5);
                    } else if (index == 61) {
                        stream.Write (new byte[] { 0xf8 }, 0, 5);
                    } else {
                        stream.Write (new byte[] { (byte) index }, 2, 6);
                    }
                }
                count++;
            }

            // Dump out the bytes
            byte[] result = new byte[stream.Position / 8];
            stream.Seek (0, System.IO.SeekOrigin.Begin);
            stream.Read (result, 0, result.Length * 8);
            return result;
        }

        #endregion Codifice

        #region Funzioni Interne

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Database configuration string. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ///
        /// <returns>   A String. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        internal static System.Data.Common.DbConnection DbConnection () {
            System.Data.Common.DbConnection ret = null;
            try {
                switch (Impostazioni.Impostazioni.Istanza.ImpostazioniDatabase.Database) {
                    case Impostazioni.ImpostazioniDatabase.TipoDatabase.SQLite:
                        var conBuilder = new System.Data.SQLite.SQLiteConnectionStringBuilder ();
                        conBuilder.DataSource = getDatabaseFileName ();
                        conBuilder.ForeignKeys = true;

                        ret = new System.Data.SQLite.SQLiteConnection (conBuilder.ConnectionString);
                        break;

                    case Impostazioni.ImpostazioniDatabase.TipoDatabase.MySql:
                        var connBuilder = new MySql.Data.MySqlClient.MySqlConnectionStringBuilder ();
                        connBuilder.Server = "";
                        ret = new MySql.Data.MySqlClient.MySqlConnection (connBuilder.ConnectionString);
                        break;
                }
            } catch (Exception ex) {
                Console.WriteLine ("Errore {0}", ex);
                ret = new System.Data.SqlClient.SqlConnection (@"data source=(LocalDb)\MSSQLLocalDB;initial catalog=LYTM.Core.Database.Database;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework");
            }
            return ret;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottieni il prefisso della tabella partendo dall'ID del plugin. </summary>
        ///
        /// <remarks>   Luca, 27/01/2018. </remarks>
        ///
        /// <param name="ID">
        ///     ID del plugin.
        /// </param>
        ///
        /// <returns>   Il prefisso tabella. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        internal static String GetPrefissoTabella (Guid ID) {
            String ret;
            // Controllo se ID già presente in db
            using (var db = new Database.DbContext ()) {
                // Controllo se già presente in db
                var plugins = db.Plugins.ToList ().Where (x => x.ID == ID).ToList ();
                var plg = plugins.FirstOrDefault (x => x.ID == ID);
                if (plg == null) {
                    // Non presente, lo aggiungo
                    int lunghezzaPrefisso = new Random ().Next (1, 25);
                    plg = new Database.Tabelle.Plugin ();
                    plg.ID = ID;
                    plg.Prefisso = MlkPwgen.PasswordGenerator.Generate (lunghezzaPrefisso, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
                    ret = plg.Prefisso;
                    db.Plugins.Add (plg);
                    db.SaveChanges ();
                } else {
                    ret = plg.Prefisso;
                }
            }

            return ret;
        }

        #endregion Funzioni Interne
    }
}