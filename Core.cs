﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Core.cs
//
// summary:	Implements the core class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LYTM.Core {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Classe che gestisce il funzionamento base dell'applicazione. </summary>
    ///
    /// <remarks>   Luca, 31/12/2017. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public class Core {

        #region Variabili

        /// <summary>   The instance. </summary>
        private static Core _instance;

        /// <summary>   The logger. </summary>
        private ILog logger = LogManager.GetLogger (typeof (Core));

        /// <summary>   The working thread. </summary>
        private System.Threading.Thread workingThread;

        /// <summary>   Indica se il thread è in lavoro. </summary>
        private bool working;

        /// <summary>   The coda lavoro. </summary>
        private Queue<WorkerItem> codaLavoro;

        /// <summary>   Elenco dei plugin. </summary>
        private System.Collections.ObjectModel.ObservableCollection<Plugin.IPlugin> _plugins;

        /// <summary>   Api Key dell'applicazione. </summary>
        private String appBugsnagKey;

        /// <summary>   Client BugSnag per .dll Core. </summary>
        private Bugsnag.Clients.BaseClient coreBugsnagClient;

        /// <summary>   Indica se è stato inizializzato. </summary>
        private bool inizializzato;

        #endregion Variabili

        #region Costruttori

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Constructor that prevents a default instance of this class from being created.
        /// </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private Core () {
            _plugins = new System.Collections.ObjectModel.ObservableCollection<Plugin.IPlugin> ();
            // Inizializzo il thread di lavoro
            workingThread = new System.Threading.Thread (work);
            workingThread.Name = System.Reflection.Assembly.GetExecutingAssembly ().GetName ().Name + " Working Thread";
            codaLavoro = new Queue<WorkerItem> ();
            System.Windows.Application.Current.Exit += Application_Exit;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            // Inizializzazione BugSnag
            coreBugsnagClient = new Bugsnag.Clients.BaseClient (ApiKey.BugsnagApiKey);
            // Disabilito Auto-Invia
            coreBugsnagClient.StopAutoNotify ();

            inizializzato = false;
        }

        #endregion Costruttori

        #region Property

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets the instance. </summary>
        ///
        /// <value> The instance. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static Core Instance {
            get {
                if (_instance == null)
                    _instance = new Core ();

                return _instance;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Elenco dei plugin disponibili </summary>
        ///
        /// <value> The plugins. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public System.Collections.ObjectModel.ReadOnlyObservableCollection<Plugin.IPlugin> Plugins {
            get {
                return new System.Collections.ObjectModel.ReadOnlyObservableCollection<Plugin.IPlugin> (_plugins);
            }
        }

        #endregion Property

        #region Metodi Pubblici

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializzas this object. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public bool Inizializza () {
            return inizializza (null);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializzas this object. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ///
        /// <param name="DbContextApp">
        ///     Elenco dei DbContext dell'applicazione application.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public bool Inizializza<T> () where T : Database.MyDatabase, new() {
            return inizializza (new T ());
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializzas this object. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public bool Inizializza (String appBugsnagKey) {
            this.appBugsnagKey = appBugsnagKey;
            return inizializza (null);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializzas this object. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ///
        /// <param name="DbContextApp">
        ///     Elenco dei DbContext dell'applicazione application.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public bool Inizializza<T> (String appBugsnagKey) where T : Database.MyDatabase, new() {
            this.appBugsnagKey = appBugsnagKey;
            return inizializza (new T ());
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Avvia il Thread di lavoro. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public void Avvia () {
            if (!inizializzato) {
                throw new InvalidOperationException ("Impossibile avviare senza aver inizializzato");
            }

            working = true;
            logger.Info ("Avvio Thread lavoro");
            workingThread.Start ();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Arresta il Thread di lavoro. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public void Arresta () {
            working = false;
            logger.Info ("Arresto Thread lavoro");
            if (!workingThread.Join (new TimeSpan (0, 5, 0))) {
                logger.Warn ("WorkingThread non arrestato dopo 5 minuti");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Aggiungi lavoro. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ///
        /// <param name="plugin">
        ///     The plugin.
        /// </param>
        /// <param name="action">
        ///     Lavoro da eseguire.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public void AggiungiLavoro (Plugin.IPlugin plugin, Action action) {
            lock (codaLavoro) {
                codaLavoro.Enqueue (new WorkerItem (plugin, action));
            }
        }

        public void ApriPagina (System.Windows.Controls.UserControl pagina) {
            try {
                ((UI.Finestre.MainWindow) System.Windows.Application.Current.MainWindow).Content = pagina;
            } catch (Exception ex) {
                logger.Error ("Errore nell'inizializzazione della finestra", ex);
                throw ex;
            }
        }

        #endregion Metodi Pubblici

        #region Metodi Privati

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Thread di lavoro. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void work () {
            logger.Info ("Thread lavoro avviato");
            WorkerItem lavoroCorrente = null;
            while (working) {
                try {
                    lavoroCorrente = null;
                    lock (codaLavoro) {
                        if (codaLavoro.Count > 0)
                            lavoroCorrente = codaLavoro.Dequeue ();
                    }
                    if (lavoroCorrente != null) {
                        logger.InfoFormat ("Avvio lavoro per plugin {0}", lavoroCorrente.Plugin.Nome);
                        lavoroCorrente.Codice ();
                    }
                } catch (Exception e) {
                    logger.Error (e);
                } finally {
                }
            }
            logger.Info ("Thread lavoro terminato");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializza librerie. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ///
        /// <exception cref="DllNotFoundException">
        ///     Thrown when a DLL Not Found error condition occurs.
        /// </exception>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void inizializzaLibrerie () {
            String outPath = Utility.getAppDataFolder ();
            String piattaforma;

            if (Environment.Is64BitProcess)
                piattaforma = "x64";
            else
                piattaforma = "x86";

            outPath = System.IO.Path.Combine (outPath, "libs", piattaforma);

            if (!System.IO.Directory.Exists (outPath))
                System.IO.Directory.CreateDirectory (outPath);

            //SQLite3
            String sqliteDllName = System.IO.Path.Combine (outPath, "sqlite3.dll");
            using (System.IO.Stream stm = System.Reflection.Assembly.GetExecutingAssembly ().GetManifestResourceStream ("LYTM.Core.Resources.Lib." + piattaforma + ".sqlite3.dll")) {
                try {
                    using (System.IO.Stream outFile = System.IO.File.Create (sqliteDllName)) {
                        const int sz = 4096;
                        byte[] buf = new byte[sz];
                        while (true) {
                            int nRead = stm.Read (buf, 0, sz);
                            if (nRead < 1)
                                break;
                            outFile.Write (buf, 0, nRead);
                        }
                    }
                } catch (Exception e) {
                    // This may happen if another process has already created and loaded the file.
                    // Since the directory includes the version number of this assembly we can
                    // assume that it's the same bits, so we just ignore the excecption here and
                    // load the DLL.
                    logger.Error (e);
                }
            }
            IntPtr h = LoadLibrary (sqliteDllName);
            if (h == IntPtr.Zero) {
                Exception e = new System.ComponentModel.Win32Exception ();
                throw new DllNotFoundException ("Unable to load library: " + "sqlite3.dll" + " from " + outPath, e);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Configura log4net. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void configuraLog4Net () {
            log4net.GlobalContext.Properties["Assembly.Version"] = System.Diagnostics.FileVersionInfo.GetVersionInfo (System.Reflection.Assembly.GetExecutingAssembly ().Location).FileVersion;
            System.Management.ManagementObjectSearcher mos = new System.Management.ManagementObjectSearcher ("select * from Win32_OperatingSystem");
            foreach (System.Management.ManagementObject managementObject in mos.Get ()) {
                log4net.GlobalContext.Properties["WindowsVersion"] = managementObject["Caption"].ToString ();   //Display operating system caption
                log4net.GlobalContext.Properties["OSArchitecture"] = managementObject["OSArchitecture"].ToString ();   //Display operating system architecture.
                if (managementObject["CSDVersion"] != null)
                    log4net.GlobalContext.Properties["CSDVersion"] = managementObject["CSDVersion"].ToString ();     //Display operating system version.
                else
                    log4net.GlobalContext.Properties["CSDVersion"] = "";
                log4net.GlobalContext.Properties["RAM"] = Utility.sizeSuffix (Convert.ToInt64 (managementObject["TotalVisibleMemorySize"].ToString ()) * 1024, 2);
            }

            Microsoft.Win32.RegistryKey processor_name = Microsoft.Win32.Registry.LocalMachine.OpenSubKey (@"Hardware\Description\System\CentralProcessor\0", Microsoft.Win32.RegistryKeyPermissionCheck.ReadSubTree);   //This registry entry contains entry for processor info.
            if (processor_name != null) {
                if (processor_name.GetValue ("ProcessorNameString") != null) {
                    log4net.GlobalContext.Properties["Processore"] = processor_name.GetValue ("ProcessorNameString");   //Display processor info.
                }
            }
            log4net.GlobalContext.Properties["logFile"] = Utility.getLogFileName ();
            using (var stream = System.Reflection.Assembly.GetExecutingAssembly ().GetManifestResourceStream ("LYTM.Core.Resources.log4net.xml")) {
                using (var reader = new System.IO.StreamReader (stream)) {
                    var xmlDoc = new System.Xml.XmlDocument ();
                    xmlDoc.LoadXml (reader.ReadToEnd ());
                    log4net.Config.XmlConfigurator.Configure (xmlDoc.DocumentElement);
                }
            }

            log4net.Repository.Hierarchy.Hierarchy hier = (log4net.Repository.Hierarchy.Hierarchy) log4net.LogManager.GetRepository ();
            switch (Impostazioni.Impostazioni.Istanza.LivelloLog) {
                case 0:
                    hier.Root.Level = log4net.Core.Level.All;
                    break;

                case 1:
                    hier.Root.Level = log4net.Core.Level.Debug;
                    break;

                case 2:
                    hier.Root.Level = log4net.Core.Level.Info;
                    break;

                case 3:
                    hier.Root.Level = log4net.Core.Level.Warn;
                    break;

                case 4:
                    hier.Root.Level = log4net.Core.Level.Error;
                    break;

                case 5:
                    hier.Root.Level = log4net.Core.Level.Fatal;
                    break;

                default:
                    break;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Carica plugin. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void caricaPlugin () {
            logger.Info ("Cerco e carico plugins");

            // Ottengo tutte le cartelle di plugin disponibili
            String[] pluginsDir = System.IO.Directory.GetDirectories (Utility.getPluginDir ());
            // Cerco plugin nelle varie cartelle
            foreach (var plugin in pluginsDir) {
                bool caricato = false;
                var nomeDll = System.IO.Path.GetFileName (plugin);
                logger.InfoFormat ("Carico plugin in subfolder {0}", nomeDll);

                nomeDll = System.IO.Path.Combine (plugin, nomeDll + ".dll");
                try {
                    System.Reflection.Assembly asm = System.Reflection.Assembly.Load (System.Reflection.AssemblyName.GetAssemblyName (nomeDll));
                    if (asm != null) {
                        var tipi = asm.GetTypes ();
                        foreach (var tipo in tipi)
                            if (tipo != null && !tipo.IsInterface && !tipo.IsAbstract && typeof (Plugin.IPlugin).IsAssignableFrom (tipo)) {
                                // Creo il plugin e lo aggiungo all'elenco
                                var pluginRef = (Plugin.IPlugin) Activator.CreateInstance (tipo);
                                _plugins.Add (pluginRef);
                                logger.InfoFormat ("Plugin {0} è stato caricato correttamente", pluginRef.Nome);
                                caricato = true;
                                // Aggiungo i downloader
                                if (pluginRef.HasDownloader)
                                    Cache.CacheManager.Istance.AddDownloadPlugins (pluginRef.Downloader);
                            }
                    }
                } catch (System.Reflection.ReflectionTypeLoadException e) {
                    logger.Error (e);
                    foreach (Exception ex in e.LoaderExceptions) {
                        logger.Error (ex);
                    }
                } catch (Exception e) {
                    logger.Error (e);
                }
                if (!caricato)
                    logger.InfoFormat ("Plugin non presente in {0}", plugin);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Inizializza Core. </summary>
        ///
        /// <remarks>   Luca, 04/02/2018. </remarks>
        ///
        /// <exception cref="InvalidOperationException">
        ///     Lanciata quando si tenta di inizializzare un'istanza già inizializzata
        /// </exception>
        ///
        /// <param name="appDbContext">
        ///     Context for the application database.
        /// </param>
        ///
        /// <returns>   true if it succeeds, false if it fails. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private bool inizializza (Database.MyDatabase appDbContext) {
            if (inizializzato)
                throw new InvalidOperationException ("Core già inizializzato");
            //Estraggo e carico Dll
            inizializzaLibrerie ();
            //Inizializzo log4net
            configuraLog4Net ();
            //Carico Plugin
            caricaPlugin ();
            // Carico Risorse Applicazione
            logger.Info ("Caricamento Risorse Applicazione");
            System.Windows.Application.Current.Resources.MergedDictionaries.Add (new System.Windows.ResourceDictionary () { Source = new Uri (@"pack://application:,,,/" + System.Reflection.Assembly.GetExecutingAssembly ().GetName ().Name + ";component/Resources/RisorseApplicazione.xaml", UriKind.RelativeOrAbsolute) });
            //Controllo Database
            UI.Finestre.ControlloDatabase ctrl = new UI.Finestre.ControlloDatabase ();
            ctrl.ElencoDatabase.Add (new Database.DbContext ());
            if (appDbContext != null)
                ctrl.ElencoDatabase.Add (appDbContext);
            foreach (var plugin in Plugins) {
                var context = plugin.DbContext;
                if (context != null) {
                    var type = context.GetType ();
                    ctrl.ElencoDatabase.Add (plugin.DbContext);
                }
            }
            var res = ctrl.ShowDialog ();
            if (!res.HasValue || !res.Value) {
                logger.Error ("Errore nel controllo dei db, esco");
                System.Windows.MessageBox.Show ("Errore nel controllo dei db, esco", "Errore", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                System.Windows.Application.Current.Shutdown (Exceptions.ErrorCode.ErroreControlloDatabase);
                return false;
            }

            System.Windows.Application.Current.Activated += Application_Activated;

            inizializzato = true;

            return true;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Loads a library. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ///
        /// <param name="lpFileName">
        ///     Filename of the file.
        /// </param>
        ///
        /// <returns>   The library. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [System.Runtime.InteropServices.DllImport ("kernel32", SetLastError = true, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        private static extern IntPtr LoadLibrary (string lpFileName);

        #endregion Metodi Privati

        #region Eventi

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by Application for exit events. </summary>
        ///
        /// <remarks>   Luca, 13/01/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Exit event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void Application_Exit (object sender, System.Windows.ExitEventArgs e) {
            if (working) {
                this.Arresta ();
            }

            String footer = @"#-----------------------------------------------------------
# Codice Uscita: %property{CodiceUscita}
# Max Ram Usata: %property{RAM.Usata}
# Tempo Processore Totale: %property{TempoProcessore}
#-----------------------------------------------------------
";

            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess ();

            footer = footer.Replace ("%property{RAM.Usata}", Utility.sizeSuffix (proc.PeakWorkingSet64, 2));
            footer = footer.Replace ("%property{TempoProcessore}", proc.TotalProcessorTime.ToString ());
            footer = footer.Replace ("%property{CodiceUscita}", "0x" + e.ApplicationExitCode.ToString ("X8"));

            log4net.Repository.Hierarchy.Hierarchy hier = log4net.LogManager.GetRepository () as log4net.Repository.Hierarchy.Hierarchy;
            if (hier != null) {
                var appender = (ZipFileAppender) hier.GetAppenders ().Where (app => app.Name.Equals ("LogFileAppender", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault ();
                appender.Layout = new log4net.Layout.PatternLayout ("% date[% thread] % -5level % logger - % message % newline") { Footer = footer };
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by Application for activated events. </summary>
        ///
        /// <remarks>   Luca, 03/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void Application_Activated (object sender, EventArgs e) {
            // Rimuovo gestione dell'evento, mi interessa solo la prima volta per associare alla finestra la lista dei plugin
            System.Windows.Application.Current.Activated -= Application_Activated;
            try {
                ((UI.Finestre.MainWindow) System.Windows.Application.Current.MainWindow).ElementiMenuLaterale = Plugins;
            } catch (Exception ex) {
                logger.Error ("Errore nell'inizializzazione della finestra", ex);
                throw ex;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by CurrentDomain for unhandled exception events. </summary>
        ///
        /// <remarks>   Luca, 04/02/2018. </remarks>
        ///
        /// <param name="sender">
        ///     Source of the event.
        /// </param>
        /// <param name="e">
        ///     Unhandled exception event information.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void CurrentDomain_UnhandledException (object sender, UnhandledExceptionEventArgs e) {
            Exception error = (Exception) e.ExceptionObject;

            // Log su file
            logger.Error ("Errore non gestito", error);

            // Arresto Core se in esecuzione
            if (working) {
                Arresta ();
            }

            var metaData = new Bugsnag.Metadata ();
            var listaPlugin = _plugins.Select (x => new Plugin.BugsnagPluginMetadata () { ID = x.ID, Autore = x.Autore, Nome = x.Nome, Versione = x.Versione, WebPage = x.WebPage }).ToList ();
            metaData.AddToTab ("Plugin Installati", "Lista Plugin", listaPlugin);

            // Versioni .dll varie
            var assemblies = System.Reflection.Assembly.GetExecutingAssembly ().GetReferencedAssemblies ();
            for (int i = 0; i < assemblies.Length; i++) {
                metaData.AddToTab ("Versioni Reference Core", assemblies[i].Name, assemblies[i].Version.ToString ());
            }

            // Impostazione severity invio su bugsnag Legio YouTube Manager
            Bugsnag.Severity severity;
            if (error.Source == "LYTM.Core") { // Bug su LYTM.Core.dll, errore critico
                severity = Bugsnag.Severity.Error;
            } else if (error.Source == System.Reflection.Assembly.GetEntryAssembly ().GetName ().Name) { // Bug su applicazione
                if (appBugsnagKey == ApiKey.BugsnagApiKey) { // Applicazione condivide segnalazioni su Legio YouTube Manager
                    severity = Bugsnag.Severity.Error;
                } else { // Applicazione ha bugsnag per conto suo
                    severity = Bugsnag.Severity.Info;
                }
            } else { // Bug su qualche plugin
                severity = Bugsnag.Severity.Info;
            }

            coreBugsnagClient.Notify (error, severity, metaData);
        }

        #endregion Eventi

        #region Classi Interne

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   A worker item. </summary>
        ///
        /// <remarks>   Luca, 31/12/2017. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private class WorkerItem {
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            /// <summary>   Gets or sets the plugin. </summary>
            ///
            /// <value> The plugin. </value>
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            public Plugin.IPlugin Plugin { get; private set; }

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            /// <summary>   Gets or sets the codice. </summary>
            ///
            /// <value> The codice. </value>
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            public Action Codice { get; private set; }

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            /// <summary>   Constructor. </summary>
            ///
            /// <remarks>   Luca, 31/12/2017. </remarks>
            ///
            /// <param name="plugin">
            ///     The plugin.
            /// </param>
            /// <param name="codice">
            ///     The codice.
            /// </param>
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            public WorkerItem (Plugin.IPlugin plugin, Action codice) {
                Plugin = plugin;
                Codice = codice;
            }
        }

        #endregion Classi Interne
    }
}