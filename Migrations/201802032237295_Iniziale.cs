namespace LYTM.Core.Migrations {

    using System;
    using System.Data.Entity.Migrations;

    public partial class Iniziale : DbMigration {

        public override void Up () {
            CreateTable (
                "dbo.Caches",
                c => new {
                    ID = c.Int (nullable: false, identity: true),
                    Url = c.String (nullable: false),
                    FileName = c.String (nullable: false),
                    Scaricato = c.Boolean (nullable: false, defaultValue: true),
                    UltimoDownload = c.DateTime (nullable: false),
                    UltimoUtilizzo = c.DateTime (nullable: false),
                    InUso = c.Boolean (nullable: false, defaultValue: false),
                })
                .PrimaryKey (t => t.ID)
                .Index (t => t.Url);

            CreateTable (
                "dbo.Plugins",
                c => new {
                    ID = c.Guid (nullable: false),
                    Prefisso = c.String (nullable: false),
                })
                .PrimaryKey (t => t.ID);
        }

        public override void Down () {
            DropIndex ("dbo.Caches", new[] { "Url" });
            DropTable ("dbo.Plugins");
            DropTable ("dbo.Caches");
        }
    }
}