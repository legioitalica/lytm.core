namespace LYTM.Core.Migrations {

    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LYTM.Core.Database.DbContext> {

        public Configuration () {
            AutomaticMigrationsEnabled = true;
            if (LYTM.Core.Impostazioni.Impostazioni.Istanza.ImpostazioniDatabase.Database == LYTM.Core.Impostazioni.ImpostazioniDatabase.TipoDatabase.SQLite) {
                SetSqlGenerator ("System.Data.SQLite", new System.Data.SQLite.EF6.Migrations.SQLiteMigrationSqlGenerator ());
            }
        }

        protected override void Seed (LYTM.Core.Database.DbContext context) {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}