namespace LYTM.Core.Migrations {

    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ConfigurationBaseContext : DbMigrationsConfiguration<LYTM.Core.Database.MyDatabase> {

        public ConfigurationBaseContext () {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed (LYTM.Core.Database.MyDatabase context) {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}