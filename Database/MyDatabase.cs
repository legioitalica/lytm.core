﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Database\Database.cs
//
// summary:	Implements the database class
////////////////////////////////////////////////////////////////////////////////////////////////////

namespace LYTM.Core.Database {

    using System;
    using System.Data.Entity;
    using System.Linq;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Database base dell'applicazione. </summary>
    ///
    /// <remarks>   Luca, 12/01/2018. </remarks>
    ///
    /// <seealso cref="T:System.Data.Entity.DbContext"/>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    [DbConfigurationType (typeof (DbConfiguration))]
    public class MyDatabase : System.Data.Entity.DbContext {

        #region Costanti

        private static readonly String[] ELENCO_NOMI_TABELLE = {
            "Cache",
            "Plugin"
        };

        #endregion Costanti

        // Il contesto è stato configurato per utilizzare una stringa di connessione 'Database' dal file di configurazione
        // dell'applicazione (App.config o Web.config). Per impostazione predefinita, la stringa di connessione è destinata al
        // database 'LYTM.Core.Database.MyDatabase' nell'istanza di LocalDb.
        //
        // Per destinarla a un database o un provider di database differente, modificare la stringa di connessione 'Database'
        // nel file di configurazione dell'applicazione.

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Default constructor. </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        internal MyDatabase () : this (Guid.Empty) {
        }

        public MyDatabase (Guid ID) : base (Utility.DbConnection (), true) {
            this.Plugin = ID;

            Database.Log = s => {
                log4net.ILog logger = log4net.LogManager.GetLogger (typeof (MyDatabase));
                logger.Debug (s);
            };
            Database.SetInitializer<MyDatabase> (new MigrateDatabaseToLatestVersion<MyDatabase, Migrations.ConfigurationBaseContext> (true));
        }

        public MyDatabase (Guid ID, String connectionString) : base (connectionString) {
            this.Plugin = ID;

            Database.Log = s => {
                log4net.ILog logger = log4net.LogManager.GetLogger (typeof (MyDatabase));
                logger.Debug (s);
            };
            Database.SetInitializer<MyDatabase> (new MigrateDatabaseToLatestVersion<MyDatabase, Migrations.ConfigurationBaseContext> (true));
        }

        #region Override

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Tale metodo viene chiamato dopo l'inizializzazione del modello di un contesto derivato,
        ///     ma prima che il modello sia stato bloccato e utilizzato per inizializzare il
        ///     contesto.L'implementazione predefinita di questo metodo non esegue alcuna operazione, ma
        ///     è possibile eseguirne l'override in una classe derivata in modo da poter configurare
        ///     ulteriormente il modello prima che venga bloccato.
        /// </summary>
        ///
        /// <remarks>   Luca, 12/01/2018. </remarks>
        ///
        /// <param name="modelBuilder">
        ///     Generatore che definisce il modello per il contesto creato.
        /// </param>
        ///
        /// <seealso cref="M:System.Data.Entity.DbContext.OnModelCreating(DbModelBuilder)"/>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        protected override void OnModelCreating (DbModelBuilder modelBuilder) {
            base.OnModelCreating (modelBuilder);
            var ser = System.Data.Entity.Design.PluralizationServices.PluralizationService.CreateService (new System.Globalization.CultureInfo ("en-US"));
            if (Plugin != Guid.Empty) {
                String prefisso = Utility.GetPrefissoTabella (Plugin);
                modelBuilder.Types ().Configure (t => {
                    String[] split = System.Text.RegularExpressions.Regex.Split (t.ClrType.Name, @"(?<!^)(?=[A-Z])");
                    t.ToTable (prefisso + "_" + String.Join (" ", split));
                });
            } else
                modelBuilder.Types ().Configure (t => t.ToTable (ser.Pluralize (t.ClrType.Name)));
        }

        #endregion Override

        #region Property

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Plugin proprietario del DbContext. </summary>
        ///
        /// <value> The plugin. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        internal Guid Plugin { get; set; }

        #endregion Property
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}