﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Database {

    internal class DbContext : MyDatabase {

        #region Cotruttori

        static DbContext () {
            System.Data.Entity.Database.SetInitializer<DbContext> (new MigrateDatabaseToLatestVersion<DbContext, Migrations.Configuration> (true));
        }

        public DbContext () : base () {
        }

        #endregion Cotruttori

        #region DbSet

        // Aggiungere DbSet per ogni tipo di entità che si desidera includere nel modello. Per ulteriori informazioni
        // sulla configurazione e sull'utilizzo di un modello Code, vedere http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the dati cache. </summary>
        ///
        /// <value> The dati cache. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public virtual DbSet<Tabelle.Cache> DatiCache { get; set; }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the plugins. </summary>
        ///
        /// <value> The plugins. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public virtual DbSet<Tabelle.Plugin> Plugins { get; set; }

        #endregion DbSet
    }
}