﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.Core.Database {

    internal class DbConfiguration : System.Data.Entity.DbConfiguration {

        public DbConfiguration () {
            try {
                switch (Impostazioni.Impostazioni.Istanza.ImpostazioniDatabase.Database) {
                    case Impostazioni.ImpostazioniDatabase.TipoDatabase.SQLite:
                        SetProviderFactory ("System.Data.SQLite", System.Data.SQLite.SQLiteFactory.Instance);
                        SetProviderFactory ("System.Data.SQLite.EF6", System.Data.SQLite.EF6.SQLiteProviderFactory.Instance);
                        SetProviderServices ("System.Data.SQLite", (DbProviderServices) System.Data.SQLite.EF6.SQLiteProviderFactory.Instance.GetService (typeof (DbProviderServices)));
                        break;

                    case Impostazioni.ImpostazioniDatabase.TipoDatabase.MySql:
                        SetProviderFactory ("MySql.Data.MySqlClient", MySql.Data.MySqlClient.MySqlClientFactory.Instance);
                        SetProviderServices ("MySql.Data.MySqlClient", new MySql.Data.MySqlClient.MySqlProviderServices ());
                        break;

                    case Impostazioni.ImpostazioniDatabase.TipoDatabase.SqlServer:
                        SetDefaultConnectionFactory (new System.Data.Entity.Infrastructure.SqlConnectionFactory ());
                        break;
                }
            } catch (Exception e) {
                log4net.LogManager.GetLogger (typeof (DbConfiguration)).Error (e);
            }
        }
    }
}