﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LYTM.Core.Database.Tabelle {

    internal class Cache {

        [Key, DatabaseGenerated (DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required, Index]
        public String Url { get; set; }

        [Required]
        public String FileName { get; set; }

        [Required]
        public bool Scaricato { get; set; }

        public DateTime UltimoDownload { get; set; }

        public DateTime UltimoUtilizzo { get; set; }

        public bool InUso { get; set; }
        public uint NumeroUtilizzi { get; set; }
        public uint NumeroDownload { get; set; }
    }
}