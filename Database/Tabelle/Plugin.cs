﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Database\Tabelle\Plugin.cs
//
// summary:	Implements the plugin class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.ComponentModel.DataAnnotations;

namespace LYTM.Core.Database.Tabelle {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Tabella Plugin. </summary>
    ///
    /// <remarks>   Luca, 12/01/2018. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Plugin {
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets the identifier. </summary>
        ///
        /// <value> The identifier. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Key]
        public Guid ID { get; set; }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Gets or sets il prefisso delle tabelle per il seguente plugin. </summary>
        ///
        /// <value> Il prefisso. </value>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        [Required]
        public String Prefisso { get; set; }
    }
}